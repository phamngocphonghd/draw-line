﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Root;

public class ServicesManager : Singleton<ServicesManager>
{

    #region services
    IDataService m_DataNormal;
    public static IDataService DataNormal()
    {
        Instance.Init();
        return Instance.m_DataNormal;
    }

    IDataService m_DataSecure;
    public static IDataService DataSecure()
    {
        Instance.Init();
        return Instance.m_DataSecure;
    }

    IJsonService m_Json;
    public static IJsonService Json()
    {
        Instance.Init();
        return Instance.m_Json;
    }

    ITimeService m_Time;
    public static ITimeService Time()
    {
        Instance.Init();
        return Instance.m_Time;
    }

    IAdsService m_Ads;
    public static IAdsService Ads()
    {
        Instance.Init();
        return Instance.m_Ads;
    }

    IAdsRewardService m_AdsReward;
    public static IAdsRewardService AdsReward()
    {
        Instance.Init();
        return Instance.m_AdsReward;
    }

    IInAppPurchaseService m_IAP;
    public static IInAppPurchaseService IAP()
    {
        Instance.Init();
        return Instance.m_IAP;
    }

    ILeaderBoardService m_LeaderBoard;
    public static ILeaderBoardService LeaderBoard()
    {
        Instance.Init();
        return Instance.m_LeaderBoard;
    }

    IScoreService m_Score;
    public static IScoreService Score()
    {
        Instance.Init();
        return Instance.m_Score;
    }

    #endregion

    bool isInit = false;
    bool isInitAllServices = false;

    #region init

    public void InitServices(bool isShowAds, bool isHaveIAP, bool isHaveLeaderBoard)
    {
        Init();

        if (isInitAllServices) return;
        isInitAllServices = true;

        if (isShowAds)
        {
            m_Ads = new AdmobModule();
            m_Ads.Init(adsConfig);

            m_AdsReward = new UnityAdsRewardModule();
            m_AdsReward.Init();
        }

        if (isHaveIAP)
        {
            //m_IAP = new UMInAppPurchaseModule(m_DataSecure);
            //m_IAP.ConnectInit();
        }

        if (isHaveLeaderBoard)
        {
            //m_LeaderBoard = new UMLeaderBoardModule();

            //if (Application.platform == RuntimePlatform.IPhonePlayer)
            //{
            //    m_LeaderBoard.ConnectService();
            //}
        }
        else
        {
            m_LeaderBoard = new LeaderBoardMockModule();
        }

        m_Score = new ScoreModule(m_DataSecure, m_LeaderBoard);
    }
    public AdsConfig adsConfig;
    void InitBindingInjection()
    {
        m_Json = new JsonDotNetModule();
        m_Time = new TimeModule();

        m_DataNormal = new PlayerPrefsModule(m_Json);
        m_DataSecure = new SecurePlayerPrefsModule(m_Json);
    }

    void Init()
    {
        if (isInit) return;
        isInit = true;

        InitBindingInjection();
    }

    #endregion

    #region Unity Event
    // Use this for initialization
    void Start()
    {
        Init();
    }

    void OnApplicationPause(bool isPaused)
    {
        if (isPaused)
        {
            SaveData();
        }
    }

    void OnApplicationQuit()
    {
        SaveData();
    }

    #endregion

    void SaveData()
    {
        Init();
        m_DataNormal.Save();
        m_DataSecure.Save();
    }

    public void ClearData()
    {
        Init();
        PlayerPrefs.DeleteAll();
    }
}