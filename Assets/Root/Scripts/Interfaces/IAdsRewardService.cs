﻿using System;

namespace Root
{

    public interface IAdsRewardService
    {

        void Init();

        void ShowAds(Action closeCallback = null);

        bool IsAdsAvailable();
    }

}

