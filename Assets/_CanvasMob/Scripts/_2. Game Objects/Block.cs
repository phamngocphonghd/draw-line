﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Block : MonoBehaviour, IBehaviour
{
    Cell[] m_Cells;

    public void Begin()
    {
        m_Cells = GetComponentsInChildren<Cell>();

        for (int i = 0; i < m_Cells.Length; i++)
        {
            m_Cells[i].Begin();
        }
    }

    public void FrameUpdate()
    {

    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public void TimeUpdate()
    {

    }
}
