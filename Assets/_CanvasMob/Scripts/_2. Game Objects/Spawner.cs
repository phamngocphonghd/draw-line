﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Spawner : MonoBehaviour, IBehaviour
{
    public enum SwipeDirection
    {
        None,
        Up,
        Down,
        Right,
        Left
    }

    public int currentHintCount = 0;
    public int maxHint = 0;
    Level levelGame;
    [SerializeField]
    public Cell m_lastCell;

    [SerializeField]
    List<Cell> m_ListCell;

    [SerializeField]
    List<Cell> history_CellDraw;

    public void Begin()
    {
        levelGame = new Level();
        m_ListCell = new List<Cell>();
        history_CellDraw = new List<Cell>();
    }

    public void FrameUpdate()
    {

    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public void TimeUpdate()
    {

    }

    public void SpawnTutorial()
    {
        levelGame = DataManager.instance.LoadDataTut();
        for (int i = 0; i < levelGame.L.Length; i++)
        {
            GameObject obj;
            obj = Instantiate(GameManager.instance.CellPrefabs, new Vector3(levelGame.L[i].x, -levelGame.L[i].y) + (0.25f * levelGame.w - 0.25f) * 2f * Vector3.left + (0.25f * levelGame.h - 0.25f) * 2f * Vector3.up, Quaternion.identity);
            var cell = obj.GetComponent<Cell>();
            m_ListCell.Add(cell);
            cell.xIndexInMap = levelGame.L[i].x;
            cell.yIndexInMap = levelGame.L[i].y;
            cell.m_spawner = this;
        }
        m_ListCell[0].isRootCell = true;
        history_CellDraw.Add(m_ListCell[0]);
        m_lastCell = m_ListCell[0];
        for (int i = 0; i < m_ListCell.Count; i++)
        {
            m_ListCell[i].Begin();
        }
    }

    public void SpawnMap(int level)
    {
        currentHintCount = 0;
        levelGame = DataManager.instance.LoadLevelChallenge(level);
        if (levelGame != null)
        {
            for (int i = 0; i < levelGame.L.Length; i++)
            {
                GameObject obj;
                if (levelGame.w > 5 || levelGame.h > 5)
                {
                    obj = Instantiate(GameManager.instance.CellPrefabs5, new Vector3(levelGame.L[i].x, -levelGame.L[i].y) * .75f + (0.25f * levelGame.w - 0.25f) * 2f * .75f * Vector3.left + (0.25f * levelGame.h - 0.25f) * 2f * .75f * Vector3.up, Quaternion.identity);

                }
                else
                {
                    obj = Instantiate(GameManager.instance.CellPrefabs, new Vector3(levelGame.L[i].x, -levelGame.L[i].y) + (0.25f * levelGame.w - 0.25f) * 2f * Vector3.left + (0.25f * levelGame.h - 0.25f) * 2f * Vector3.up, Quaternion.identity);

                }
                var cell = obj.GetComponent<Cell>();
                m_ListCell.Add(cell);
                cell.xIndexInMap = levelGame.L[i].x;
                cell.yIndexInMap = levelGame.L[i].y;
                cell.m_spawner = this;
            }
            m_ListCell[0].isRootCell = true;
            history_CellDraw.Add(m_ListCell[0]);
            m_lastCell = m_ListCell[0];
            for (int i = 0; i < m_ListCell.Count; i++)
            {
                m_ListCell[i].Begin();
            }

            maxHint = levelGame.L.Length / 4;
        }
    }

    public void CheckWin()
    {
        if (history_CellDraw.Count == m_ListCell.Count)
        {
            if(GameManager.instance.isTut)
            {
                GameManager.instance.WinTutorial();
                SoundManager.instance.PlaySoundLastDraw();
            }
            else
            {
                GameManager.instance.WinStage();
                SoundManager.instance.PlaySoundLastDraw();
            }
        }
    }

    public void AddCelltoHistory(Cell cell)
    {
        history_CellDraw.Add(cell);
        m_lastCell = cell;
        history_CellDraw[history_CellDraw.Count - 1].IndexInList = history_CellDraw.Count - 1;
    }

    public bool CheckIsLastDraw(Cell cell)
    {
        if (m_lastCell == cell)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public int xIndexLastCell()
    {
        return m_lastCell.xIndexInMap;
    }

    public int yIndexLastCell()
    {
        return m_lastCell.yIndexInMap;
    }

    public void ChangeLastCellDraw(Cell cell)
    {
        m_lastCell = cell;
    }

    public void ReDraw(int index)
    {
        for (int i = history_CellDraw.Count - 1; i > index; i--)
        {
            history_CellDraw[i].HideAllSprite();
            history_CellDraw[i].isDraw = false;
            history_CellDraw.RemoveAt(i);
        }

        m_lastCell = history_CellDraw[index];
        DrawMap();
        if (history_CellDraw[history_CellDraw.Count - 1].xIndexInMap == history_CellDraw[history_CellDraw.Count - 2].xIndexInMap)
        {
            if (history_CellDraw[history_CellDraw.Count - 1].yIndexInMap == history_CellDraw[history_CellDraw.Count - 1].yIndexInMap + 1)
            {
                history_CellDraw[history_CellDraw.Count - 1].SurfaceUp();
            }
            else
            {
                history_CellDraw[history_CellDraw.Count - 1].SurfaceDown();
            }
        }
        else
        {
            if (history_CellDraw[history_CellDraw.Count - 1].xIndexInMap == history_CellDraw[history_CellDraw.Count - 1].xIndexInMap + 1)
            {
                history_CellDraw[history_CellDraw.Count - 1].SurfaceLeft();
            }
            else
            {
                history_CellDraw[history_CellDraw.Count - 1].SurfaceRight();
            }
        }
    }

    public void ResetMap()
    {
        for (int i = history_CellDraw.Count - 1; i > 0; i--)
        {
            history_CellDraw[i].isDraw = false;
            history_CellDraw[i].HideAllSprite();
            history_CellDraw.RemoveAt(i);
        }

        history_CellDraw[0].HideAllSprite();
        history_CellDraw[0].spriteMiddle.gameObject.SetActive(true);
        m_lastCell = history_CellDraw[0];
    }

    public void DrawMap()
    {
        for (int i = 0; i < history_CellDraw.Count - 1; i++)
        {
            if (i == 0)
            {
                if (history_CellDraw[i + 1] != null)
                {
                    if (history_CellDraw[i].xIndexInMap == history_CellDraw[i + 1].xIndexInMap)
                    {
                        if (history_CellDraw[i].yIndexInMap == history_CellDraw[i + 1].yIndexInMap)
                        {
                            history_CellDraw[i].SurfaceDown();
                        }
                        else
                        {
                            history_CellDraw[i].SurfaceUp();
                        }
                    }
                    else if (history_CellDraw[i].yIndexInMap == history_CellDraw[i + 1].yIndexInMap)
                    {
                        if (history_CellDraw[i].xIndexInMap == history_CellDraw[i + 1].xIndexInMap + 1)
                        {
                            history_CellDraw[i].SurfaceLeft();
                        }
                        else
                        {
                            history_CellDraw[i].SurfaceRight();
                        }
                    }
                }
                else
                {
                    history_CellDraw[i].HideAllSprite();
                    history_CellDraw[i].spriteMiddle.gameObject.SetActive(true);
                }
            }
            else
            {
                if (history_CellDraw[i].xIndexInMap == history_CellDraw[i - 1].xIndexInMap)
                {
                    if (history_CellDraw[i].yIndexInMap == history_CellDraw[i - 1].yIndexInMap + 1)
                    {
                        if (history_CellDraw[i + 1] != null)
                        {
                            if (history_CellDraw[i].xIndexInMap == history_CellDraw[i + 1].xIndexInMap)
                            {
                                history_CellDraw[i].SurfaceVertical();
                            }
                            else
                            {
                                if (history_CellDraw[i].xIndexInMap == history_CellDraw[i + 1].xIndexInMap + 1)
                                {
                                    history_CellDraw[i].SurfaceLeftUp();
                                }
                                else
                                {
                                    history_CellDraw[i].SurfaceRightUp();
                                }
                            }
                        }
                        else
                        {
                            if (history_CellDraw[i].yIndexInMap == history_CellDraw[i + 1].yIndexInMap - 1)
                            {
                                history_CellDraw[i].SurfaceUp();
                            }
                            else if (history_CellDraw[i].xIndexInMap == history_CellDraw[i + 1].xIndexInMap + 1)
                            {
                                history_CellDraw[i].SurfaceDown();
                            }
                        }
                    }
                    else
                    {
                        if (history_CellDraw[i + 1] != null)
                        {
                            if (history_CellDraw[i].xIndexInMap == history_CellDraw[i + 1].xIndexInMap)
                            {
                                history_CellDraw[i].SurfaceVertical();
                            }
                            else
                            {
                                if (history_CellDraw[i].xIndexInMap == history_CellDraw[i + 1].xIndexInMap + 1)
                                {
                                    history_CellDraw[i].SurfaceLeftDown();
                                }
                                else
                                {
                                    history_CellDraw[i].SurfaceRightDown();
                                }
                            }
                        }
                        else
                        {
                            if (history_CellDraw[i].yIndexInMap == history_CellDraw[i + 1].yIndexInMap - 1)
                            {
                                history_CellDraw[i].SurfaceUp();
                            }
                            else if (history_CellDraw[i].xIndexInMap == history_CellDraw[i + 1].xIndexInMap + 1)
                            {
                                history_CellDraw[i].SurfaceDown();
                            }
                        }
                    }
                }
                else if (history_CellDraw[i].yIndexInMap == history_CellDraw[i - 1].yIndexInMap)
                {
                    if (history_CellDraw[i].xIndexInMap == history_CellDraw[i - 1].xIndexInMap + 1)
                    {
                        if (history_CellDraw[i + 1] != null)
                        {
                            if (history_CellDraw[i].yIndexInMap == history_CellDraw[i + 1].yIndexInMap)
                            {
                                history_CellDraw[i].SurfaceHorizon();
                            }
                            else
                            {
                                if (history_CellDraw[i].yIndexInMap == history_CellDraw[i + 1].yIndexInMap + 1)
                                {
                                    history_CellDraw[i].SurfaceLeftUp();
                                }
                                else
                                {
                                    history_CellDraw[i].SurfaceLeftDown();
                                }
                            }
                        }
                        else
                        {
                            if (history_CellDraw[i].yIndexInMap == history_CellDraw[i + 1].yIndexInMap - 1)
                            {
                                history_CellDraw[i].SurfaceLeft();
                            }
                            else if (history_CellDraw[i].xIndexInMap == history_CellDraw[i + 1].xIndexInMap + 1)
                            {
                                history_CellDraw[i].SurfaceRight();
                            }
                        }
                    }
                    else
                    {
                        if (history_CellDraw[i + 1] != null)
                        {
                            if (history_CellDraw[i].yIndexInMap == history_CellDraw[i + 1].yIndexInMap)
                            {
                                history_CellDraw[i].SurfaceHorizon();
                            }
                            else
                            {
                                if (history_CellDraw[i].yIndexInMap == history_CellDraw[i + 1].yIndexInMap + 1)
                                {
                                    history_CellDraw[i].SurfaceRightUp();
                                }
                                else
                                {
                                    history_CellDraw[i].SurfaceRightDown();
                                }
                            }
                        }
                        else
                        {
                            if (history_CellDraw[i].yIndexInMap == history_CellDraw[i + 1].yIndexInMap - 1)
                            {
                                history_CellDraw[i].SurfaceLeft();
                            }
                            else if (history_CellDraw[i].xIndexInMap == history_CellDraw[i + 1].xIndexInMap + 1)
                            {
                                history_CellDraw[i].SurfaceRight();
                            }
                        }
                    }
                }
            }

            history_CellDraw[i].isDraw = true;
        }

        CheckWin();
    }

    public void RecycleInWinGame()
    {
        for (int i = m_ListCell.Count - 1; i >= 0; i--)
        {
            m_ListCell[i].gameObject.Recycle();
            m_ListCell.RemoveAt(i);
        }

        history_CellDraw.Clear();
        m_ListCell.Clear();
    }

    public void ShowHint()
    {
        if (currentHintCount > maxHint) return;
        ResetMap();
        for (int i = 1; i < (currentHintCount + 1) * 5; i++)
        {
            history_CellDraw.Add(m_ListCell[i]);
            m_lastCell = m_ListCell[i];
        }
        DrawMap();
        currentHintCount++;
        //if (currentHintCount == 0)
        //{
        //    ResetMap();

        //    for (int i = 1; i < 5; i++)
        //    {
        //        history_CellDraw.Add(m_ListCell[i]);
        //    }

        //    DrawMap();
        //    currentHintCount++;
        //}
        //else if (currentHintCount <= (m_ListCell.Count / 5))
        //{
        //    for (int i = history_CellDraw.Count; i < history_CellDraw.Count + 3; i++)
        //    {
        //        history_CellDraw.Add(m_ListCell[i]);
        //    }

        //    DrawMap();
        //    currentHintCount++;
        //}
    }
}
