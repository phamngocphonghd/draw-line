﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Cell : MonoBehaviour, IBehaviour
{
    public bool isRootCell = false;
    public bool isDraw = false;
    public int xIndexInMap;
    public int yIndexInMap;
    public int IndexInList = 0;
    public Spawner m_spawner;
    public SpriteRenderer spriteMiddle;
    public SpriteRenderer spriteUp;
    public SpriteRenderer spriteDown;
    public SpriteRenderer spriteRight;
    public SpriteRenderer spriteLeft;
    Color colorSurface;
    private void Start()
    {

    }

    public void Begin()
    {
        colorSurface = UIManager.instance.basicColor;
        spriteMiddle.color = colorSurface;
        spriteUp.color = colorSurface;
        spriteRight.color = colorSurface;
        spriteLeft.color = colorSurface;
        spriteDown.color = colorSurface;

        if (isRootCell)
        {
            spriteMiddle.gameObject.SetActive(true);
            isDraw = true;
        }
    }

    public void FrameUpdate()
    {

    }

    public void TimeUpdate()
    {

    }

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    void OnMouseEnter()
    {
        if (GameManager.instance.stateGame == GameManager.StateGame.Play)
        {
            if (isDraw)
            {
                if (!isRootCell)
                {
                    if (!m_spawner.CheckIsLastDraw(this))
                    {
                        //m_spawner.ReDrawHistory(this);
                        m_spawner.ReDraw(IndexInList);
                        SoundManager.instance.PlaySoundDraw();
                    }
                }
                else
                {
                    m_spawner.ResetMap();
                    SoundManager.instance.PlaySoundDraw();
                }
            }
            else
            {
                if (xIndexInMap == m_spawner.m_lastCell.xIndexInMap)
                {
                    if (yIndexInMap == m_spawner.m_lastCell.yIndexInMap + 1)
                    {
                        m_spawner.AddCelltoHistory(this);
                        isDraw = true;
                        m_spawner.DrawMap();
                        SurfaceUp();
                        SoundManager.instance.PlaySoundDraw();
                    }
                    else if (yIndexInMap == m_spawner.m_lastCell.yIndexInMap - 1)
                    {
                        m_spawner.AddCelltoHistory(this);
                        isDraw = true;
                        m_spawner.DrawMap();
                        SurfaceDown();
                        SoundManager.instance.PlaySoundDraw();
                    }
                }
                else if (yIndexInMap == m_spawner.yIndexLastCell())
                {
                    if (xIndexInMap == m_spawner.m_lastCell.xIndexInMap + 1)
                    {
                        m_spawner.AddCelltoHistory(this);
                        isDraw = true;
                        m_spawner.DrawMap();
                        SurfaceLeft();
                        SoundManager.instance.PlaySoundDraw();
                    }
                    else if (xIndexInMap == m_spawner.m_lastCell.xIndexInMap - 1)
                    {
                        m_spawner.AddCelltoHistory(this);
                        isDraw = true;
                        m_spawner.DrawMap();
                        SurfaceRight();
                        SoundManager.instance.PlaySoundDraw();
                    }
                }
            }
        }
    }

    void OnMouseExit()
    {

    }

    public void SetColorSurface()
    {

    }

    public void HideAllSprite()
    {
        spriteMiddle.gameObject.SetActive(false);
        spriteUp.gameObject.SetActive(false);
        spriteDown.gameObject.SetActive(false);
        spriteLeft.gameObject.SetActive(false);
        spriteRight.gameObject.SetActive(false);
    }

    public void SurfaceUp()
    {
        HideAllSprite();
        spriteMiddle.gameObject.SetActive(true);
        spriteUp.gameObject.SetActive(true);
    }

    public void SurfaceDown()
    {
        HideAllSprite();
        spriteMiddle.gameObject.SetActive(true);
        spriteDown.gameObject.SetActive(true);
    }

    public void SurfaceLeft()
    {
        HideAllSprite();
        spriteMiddle.gameObject.SetActive(true);
        spriteLeft.gameObject.SetActive(true);
    }

    public void SurfaceRight()
    {
        HideAllSprite();
        spriteMiddle.gameObject.SetActive(true);
        spriteRight.gameObject.SetActive(true);
    }

    public void SurfaceHorizon()
    {
        HideAllSprite();
        spriteMiddle.gameObject.SetActive(true);
        spriteRight.gameObject.SetActive(true);
        spriteLeft.gameObject.SetActive(true);
    }

    public void SurfaceVertical()
    {
        HideAllSprite();
        spriteMiddle.gameObject.SetActive(true);
        spriteUp.gameObject.SetActive(true);
        spriteDown.gameObject.SetActive(true);
    }

    public void SurfaceLeftDown()
    {
        HideAllSprite();
        spriteMiddle.gameObject.SetActive(true);
        spriteLeft.gameObject.SetActive(true);
        spriteDown.gameObject.SetActive(true);
    }

    public void SurfaceRightDown()
    {
        HideAllSprite();
        spriteMiddle.gameObject.SetActive(true);
        spriteRight.gameObject.SetActive(true);
        spriteDown.gameObject.SetActive(true);
    }

    public void SurfaceRightUp()
    {
        HideAllSprite();
        spriteMiddle.gameObject.SetActive(true);
        spriteUp.gameObject.SetActive(true);
        spriteRight.gameObject.SetActive(true);
    }

    public void SurfaceLeftUp()
    {
        HideAllSprite();
        spriteMiddle.gameObject.SetActive(true);
        spriteUp.gameObject.SetActive(true);
        spriteLeft.gameObject.SetActive(true);
    }
}
