﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class SwipeMenu : MonoBehaviour {

	[SerializeField]
    RectTransform m_ContentPanel;
    [SerializeField]
    RectTransform[] m_Tabs;
    [SerializeField]
    RectTransform m_Center;

    float[] m_TabsPos;
    public bool IsDragging;
    float m_TabDistance;
    public int MinTabIndex = 2;
    float m_SizeTab;
    int m_TabLength;
    Vector2 startPos;
    Vector2 m_MinPos, m_MaxPos;

    public void Init()
    {
        m_SizeTab = m_Tabs[0].rect.width;
        m_TabLength = m_Tabs.Length;
        m_TabsPos = new float[m_TabLength];
        m_TabDistance = Mathf.Abs(m_Tabs[0].anchoredPosition.x - m_Tabs[1].anchoredPosition.x);
        for (int i = 0; i < m_TabLength; i++)
        {
            if (m_Tabs[i] != null)
            {
                m_TabsPos[i] = m_SizeTab * (i - m_TabLength / 2);
                m_Tabs[i].anchoredPosition = new Vector3(m_TabsPos[i], 0);
            }
        }
        m_MinPos = m_Tabs[0].anchoredPosition + m_ContentPanel.anchoredPosition;
        m_MaxPos = m_Tabs[m_TabLength - 1].anchoredPosition + m_ContentPanel.anchoredPosition;
    }

    private void Update()
    {
        if (!IsDragging)
        {
            LerpToTab(-m_TabsPos[MinTabIndex]);
        }

        //SetThemeText(minDistanceTextIndex);
    }


    public void StartDrag(BaseEventData eventData)
    {
        IsDragging = true;
        var pointerStart = (PointerEventData)eventData;
        startPos = pointerStart.position;
    }

    public void StopDrag(BaseEventData eventData)
    {
        var pointerStop = (PointerEventData)eventData;
        Vector2 endPos = pointerStop.position;
        Vector2 deltaVec = startPos - endPos;
        deltaVec = deltaVec * 159f / Screen.width;
        //Debug.Log(deltaVec);
        if (Mathf.Abs(deltaVec.x) > 15)
        {
            if (deltaVec.x < 0)
            {
                MinTabIndex--;
                if (MinTabIndex < 0)
                {
                    MinTabIndex = 0;
                }
            }
            else if (deltaVec.x > 0)
            {
                MinTabIndex++;
                if (MinTabIndex > 4)
                {
                    MinTabIndex = 4;
                }
            }
        }
        IsDragging = false;
        //if (m_MinTabIndex > 2)
        //{
        //    tabControl.OnClickTab(m_MinTabIndex + 1);
        //}
        //else
        //{
        //    tabControl.OnClickTab(m_MinTabIndex);
        //}
    }

    public void OnDrag(BaseEventData eventData)
    {
        if (m_ContentPanel.anchoredPosition.x > m_MaxPos.x)
        {
            m_ContentPanel.anchoredPosition = m_MaxPos;
            return;
        }
        if (m_ContentPanel.anchoredPosition.x < m_MinPos.x)
        {
            m_ContentPanel.anchoredPosition = m_MinPos;
            return;
        }
        IsDragging = true;
    }

    void LerpToTab(float position)
    {
        //Debug.Log(position);
        float newX = Mathf.Lerp(m_ContentPanel.anchoredPosition.x, position, Time.deltaTime * 20f);
        Vector2 newPosition = new Vector2(newX, m_ContentPanel.anchoredPosition.y);
        m_ContentPanel.anchoredPosition = newPosition;
    }

    public void SetMixTabIndex(int index)
    {
        MinTabIndex = index;
    }
}
