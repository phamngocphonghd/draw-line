﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Level{

    public int w;
    public int h;
    public CellPos[] L;
}

[Serializable]
public class CellPos
{
    public int x;
    public int y;
}