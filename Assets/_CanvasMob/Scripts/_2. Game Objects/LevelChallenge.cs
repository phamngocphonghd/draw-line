﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LevelChallenge : MonoBehaviour
{
    public Text text;
    public Button button;
    public GameObject LockObj;
    public GameObject UnlockObj;
    int level;
    void ScrollCellIndex(int idx)
    {
        level = idx;
        string name = (idx + 1).ToString();
        if (text != null)
        {
            text.text = name;
        }

        if (level < GameManager.instance.maxLevelUnlock)
        {
            SetState(true);
        }
        else
        {
            SetState(false);
        }
    }

    void SetState(bool isUnlock)
    {
        LockObj.SetActive(!isUnlock);
        UnlockObj.SetActive(isUnlock);
    }

    void Start()
    {
        button.onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        GameManager.instance.OnClickStageLevel(level);
    }
    //public GameObject Lock;
    //public GameObject UnLock;

    //private void Awake()
    //{
    //    Lock = gameObject.transform.GetChild(1).gameObject;
    //    UnLock = gameObject.transform.GetChild(0).gameObject;
    //}

    //public void ShowUnLock()
    //{
    //    Lock.SetActive(false);
    //    UnLock.SetActive(true);
    //}

    //public void ShowLock()
    //{
    //    Lock.SetActive(true);
    //    UnLock.SetActive(false);
    //}


}
