﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    string urliOS;
    string urlAndroid;
    public int currentLevel;
    public int maxLevelUnlock;
    public bool isSoundOn;
    public bool isDidTut;
    public int money;
    public bool isTut;
    int countShowAds = 3;

    #region Count Time Play
    DateTime m_StartTimePlay;
    public int timePlay;

    public void StartTimeCount()
    {
        m_StartTimePlay = DateTime.UtcNow;
    }

    public void ResetTimeCount()
    {
        timePlay = 0;
    }

    public void EndTimeCount()
    {
        var endTime = DateTime.UtcNow;
        timePlay = (int)(endTime - m_StartTimePlay).TotalSeconds;
    }
    #endregion
    public enum StateGame
    {
        Menu,
        Play,
        Pause,
        Finish
    }

    public StateGame stateGame;

    [SerializeField]
    Spawner m_Spawner;
    private void Awake()
    {
        ServicesManager.instance.InitServices(true, false, false);
        ServicesManager.Ads().LoadFullAds();
        ServicesManager.Ads().LoadBannerBot();
    }

    public GameObject CellPrefabs;
    public GameObject CellPrefabs5;
    private void Start()
    {
        Application.targetFrameRate = 60;
        LoadData();
        SoundManager.instance.SetSound();
        if (!isDidTut)
        {
            stateGame = StateGame.Play;
            UIManager.instance.ShowTutorial();
            m_Spawner.SpawnTutorial();
            isTut = true;
        }
        else
        {
            UIManager.instance.ShowMenuUI();
            stateGame = StateGame.Menu;
            isTut = false;
        }
        ServicesManager.Ads().ShowBanner();
    }

    public void LoadData()
    {
        money = DataManager.instance.GetMoney();
        isSoundOn = DataManager.instance.GetSoundOnOff();
        maxLevelUnlock = DataManager.instance.GetMaxLevelUnlock();
        isDidTut = DataManager.instance.GetDidTut();
    }

    public void SaveData()
    {
        DataManager.instance.money = money;
        DataManager.instance.m_IsSoundOn = isSoundOn;
        DataManager.instance.maxLevelUnlock = maxLevelUnlock;
        DataManager.instance.isDidTut = isDidTut;
        DataManager.instance.SaveData();
    }

    private void OnApplicationQuit()
    {
        SaveData();
    }

    private void Update()
    {
        if (stateGame == StateGame.Play)
        {
            m_Spawner.FrameUpdate();
        }
    }

    private void FixedUpdate()
    {
        if (stateGame == StateGame.Play)
        {
            m_Spawner.TimeUpdate();
        }
    }

    public void WinTutorial()
    {
        isDidTut = true;
        m_Spawner.RecycleInWinGame();
        UIManager.instance.ShowDoneTutUI();
    }

    public void WinStage()
    {
        stateGame = StateGame.Pause;
        EndTimeCount();
        if (currentLevel + 1 >= maxLevelUnlock)
        {
            maxLevelUnlock++;
            money += 300;
        }
        StartCoroutine(IE_WinGame());
    }

    WaitForSeconds winTimeDelay = new WaitForSeconds(0.5f);

    IEnumerator IE_WinGame()
    {
        yield return winTimeDelay;
        m_Spawner.RecycleInWinGame();
        UIManager.instance.ShowWinUI();
        SoundManager.instance.PlaySoundWinGame();
    }

    #region On Click Button
    public void OnClickStageLevel(int level)
    {
        if (level >= maxLevelUnlock)
        {
            UIManager.instance.ShowLockWarning();
            return;
        };
        if (!DataManager.instance.IsHasData(level)) return;
        ServicesManager.Ads().HideBanner();
        SoundManager.instance.PlaySoundClickButton();
        ResetTimeCount();
        StartTimeCount();
        stateGame = StateGame.Play;
        m_Spawner.SpawnMap(level);
        UIManager.instance.ShowPlayingUI();
        currentLevel = level;
        UIManager.instance.SetStageText(currentLevel + 1);
    }

    public void ShowFullAds()
    {
        if (countShowAds > 3)
        {
            ServicesManager.Ads().ShowFullAds();
            countShowAds = 1;
        }
        else
        {
            countShowAds++;
        }
    }

    public void OnClickNextStage()
    {
        if (!DataManager.instance.IsHasData(currentLevel)) return;
        ResetTimeCount();
        StartTimeCount();
        ShowFullAds();
        currentLevel += 1;
        m_Spawner.Begin();
        stateGame = StateGame.Play;
        m_Spawner.SpawnMap(currentLevel);
        UIManager.instance.ShowPlayingUI();
        UIManager.instance.SetStageText(currentLevel + 1);
        SoundManager.instance.PlaySoundClickButton();
        SoundManager.instance.PlaySoundStartGame();
    }

    public void OnClickHintButton()
    {
        ServicesManager.AdsReward().ShowAds(() =>
        {
            m_Spawner.ShowHint();
        });
        SoundManager.instance.PlaySoundClickButton();
    }

    public void OnClickPlayBtn()
    {
        ServicesManager.Ads().ShowBanner();
        UIManager.instance.ShowChooseLevelUI();
        SoundManager.instance.PlaySoundClickButton();
    }

    public void OnClickPauseButton()
    {
        if (stateGame == StateGame.Play)
        {
            stateGame = StateGame.Pause;
            UIManager.instance.ShowPauseUI();
            SoundManager.instance.PlaySoundClickButton();
        }
    }

    public void OnClickHomeBtn()
    {
        m_Spawner.RecycleInWinGame();
        stateGame = StateGame.Menu;
        UIManager.instance.ShowMenuUI();
        SoundManager.instance.PlaySoundClickButton();
    }

    public void OnClickRestartBtn()
    {
        m_Spawner.RecycleInWinGame();
        OnClickStageLevel(currentLevel);
        SoundManager.instance.PlaySoundClickButton();
    }

    public void OnClickSoundBtn()
    {
        if (isSoundOn)
        {
            SoundManager.instance.PlaySoundClickButton();
            isSoundOn = false;
            SoundManager.instance.SetSound();
            UIManager.instance.ShowSoundBtn();
        }
        else
        {
            isSoundOn = true;
            SoundManager.instance.SetSound();
            UIManager.instance.ShowSoundBtn();
            SoundManager.instance.PlaySoundClickButton();
        }
    }

    public void OnClickShowWarning()
    {
        UIManager.instance.ShowWarning();
        SoundManager.instance.PlaySoundClickButton();
    }

    public void OnClickHideWarning()
    {
        UIManager.instance.HideWarning();
        SoundManager.instance.PlaySoundClickButton();
    }

    public void OnClickResume()
    {
        stateGame = StateGame.Play;
        UIManager.instance.HidePause();
        SoundManager.instance.PlaySoundClickButton();
    }

    public void OnClickChooseStageBtn()
    {
        m_Spawner.RecycleInWinGame();
        ServicesManager.Ads().ShowBanner();
        UIManager.instance.ShowChooseLevelUI();
        SoundManager.instance.PlaySoundClickButton();
    }

    public void OnClickShareButton()
    {
        StartCoroutine(TakeSSAndShare());
    }

    public void OnClickShowTutorial()
    {
        isTut = true;
        m_Spawner.RecycleInWinGame();
        UIManager.instance.ShowTutorial();
        m_Spawner.SpawnTutorial();
        stateGame = StateGame.Play;
        SoundManager.instance.PlaySoundClickButton();
    }

    public void OnClickHideTutorial()
    {
        isTut = false;
        m_Spawner.RecycleInWinGame();
        UIManager.instance.HideTutorial();
        UIManager.instance.ShowPlayingUI();
        OnClickStageLevel(0);
        SoundManager.instance.PlaySoundClickButton();
    }

    public void OnClickHideLockWarning()
    {
        UIManager.instance.HideLockWarning();
    }
    #endregion

    private IEnumerator TakeSSAndShare()
    {
        yield return new WaitForEndOfFrame();

        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        string filePath = Path.Combine(Application.temporaryCachePath, "shared img.png");
        File.WriteAllBytes(filePath, ss.EncodeToPNG());

        // To avoid memory leaks
        Destroy(ss);

        new NativeShare().AddFile(filePath).SetSubject("Amazing Game").SetText("Let Play").Share();

        // Share on WhatsApp only, if installed (Android only)
        //if( NativeShare.TargetExists( "com.whatsapp" ) )
        //	new NativeShare().AddFile( filePath ).SetText( "Hello world!" ).SetTarget( "com.whatsapp" ).Share();
    }
}
