﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
public class UIManager : Singleton<UIManager>
{
    public Color basicColor;
    ScaleAnim anim;
    public GameObject SoundOn;
    public GameObject SoundOff;
    public GameObject SoundOnMenu;
    public GameObject SoundOffMenu;
    public GameObject TutorialUI;
    public GameObject MenuUI;
    public GameObject ChooseLevelUI;
    public GameObject PlayingUI;
    public GameObject WinUI;
    public GameObject PauseUI;
    public GameObject PlayBtn;
    public GameObject WarningUI;
    public GameObject DoneTutorialUI;
    public GameObject LockWarningUI;
    public Text levelInWinUI;
    public Text stageText;
    public Text money;
    public Text timePlayText;
    public LevelChallenge[] levelUnLock;

    [SerializeField]
    LoopVerticalScrollRect m_LevelsScrollRect;
    private void Start()
    {
        anim = PlayBtn.GetComponent<ScaleAnim>();
        anim.StartAnim();
    }

    public void ShowMoneyText()
    {
        money.text = GameManager.instance.money.ToString();
    }
    
    public void SetStageText(int level)
    {
        stageText.text = "STAGE " + level.ToString();
    }

    public void ShowDoneTutUI()
    {
        DoneTutorialUI.SetActive(true);
    }

    public void ShowWarning()
    {
        WarningUI.SetActive(true);
    }

    public void HideWarning()
    {
        WarningUI.SetActive(false);
    }

    public void ShowSoundBtn()
    {
        if (GameManager.instance.isSoundOn)
        {
            SoundOn.SetActive(true);
            SoundOff.SetActive(false);
            SoundOnMenu.SetActive(true);
            SoundOffMenu.SetActive(false);
        }
        else
        {
            SoundOn.SetActive(false);
            SoundOff.SetActive(true);
            SoundOnMenu.SetActive(false);
            SoundOffMenu.SetActive(true);
        }
    }

    public void HideAll()
    {
        MenuUI.SetActive(false);
        ChooseLevelUI.SetActive(false);
        PlayingUI.SetActive(false);
        WinUI.SetActive(false);
        PauseUI.SetActive(false);
        TutorialUI.SetActive(false);
        DoneTutorialUI.SetActive(false);
        LockWarningUI.SetActive(false);
    }

    public void ShowTutorial()
    {
        HideAll();
        TutorialUI.SetActive(true);
    }

    public void HideTutorial()
    {
        HideAll();
        TutorialUI.SetActive(false);
    }

    public void ShowMenuUI()
    {
        HideAll();
        MenuUI.SetActive(true);
        ShowMoneyText();
        SoundManager.instance.PlaySoundBackGround();
        ShowSoundBtn();
    }

    public void ShowChooseLevelUI()
    {
        HideAll();
        ChooseLevelUI.SetActive(true);
        m_LevelsScrollRect.RefreshCells();
        //m_LevelsScrollRect.RefillCellsFromEnd((GameManager.instance.maxLevelUnlock));
        m_LevelsScrollRect.ScrollToCell(GameManager.instance.maxLevelUnlock - 1, 10000f);
    }

    public void ShowPlayingUI()
    {
        HideAll();
        PlayingUI.SetActive(true);
    }

    public void ShowWinUI()
    {
        WinUI.SetActive(true);
        timePlayText.text = GameManager.instance.timePlay.ToString() + "s";
        levelInWinUI.text = "Lv "+ (GameManager.instance.currentLevel + 1).ToString();
    }

    public void ShowPauseUI()
    {
        PauseUI.SetActive(true);
        ShowSoundBtn();
    }

    public void HidePause()
    {
        PauseUI.SetActive(false);
    }

    public void ShowLockWarning()
    {
        LockWarningUI.SetActive(true);
    }

    public void HideLockWarning()
    {
        LockWarningUI.SetActive(false);
    }
}
