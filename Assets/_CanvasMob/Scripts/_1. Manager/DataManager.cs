﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : Singleton<DataManager>
{
    public Level dataLevel;
    public Level dataTut;
    public bool m_IsSoundOn;
    public int maxLevelUnlock;
    public int money;
    public bool isDidTut;

    private void Start()
    {

    }

    public Level LoadDataTut()
    {
        var ta = Resources.Load("tutorial");
        dataTut = JsonUtility.FromJson<Level>(ta.ToString());
        return dataTut;
    }

    public Level LoadLevelChallenge(int level)
    {
        string path;
        if (level < 80)
        {
            path = "lvl" + level.ToString();
        }
        else if (level < 120)
        {
            path = "lp2/lvl" + (level % 8).ToString();
        }
        else if (level < 160)
        {
            path = "lp3/lvl" + (level % 12).ToString();
        }
        else if (level < 200)
        {
            path = "lp4/lvl" + (level % 16).ToString();
        }
        else if (level < 240)
        {
            path = "lp5/lvl" + (level % 20).ToString();
        }
        else if (level < 280)
        {
            path = "lp6/lvl" + (level % 24).ToString();
        }
        else if (level < 320)
        {
            path = "lp7/lvl" + (level % 28).ToString();
        }
        else if (level < 360)
        {
            path = "lp8/lvl" + (level % 32).ToString();
        }
        else if (level < 400)
        {
            path = "lp9/lvl" + (level % 36).ToString();
        }
        else if (level < 440)
        {
            path = "lp10/lvl" + (level % 40).ToString();
        }
        else if (level < 480)
        {
            path = "lp11/lvl" + (level % 44).ToString();
        }
        else if (level < 520)
        {
            path = "lp12/lvl" + (level % 48).ToString();

        }
        else if (level < 560)
        {
            path = "lp13/lvl" + (level % 52).ToString();

        }
        else if (level < 600)
        {
            path = "lp14/lvl" + (level % 56).ToString();
        }
        else if (level < 640)
        {
            path = "lp15/lvl" + (level % 60).ToString();
        }
        else if (level < 680)
        {
            path = "lp16/lvl" + (level % 64).ToString();
        }
        else if (level < 720)
        {
            path = "lp17/lvl" + (level % 68).ToString();
        }
        else if (level < 760)
        {
            path = "lp18/lvl" + (level % 72).ToString();
        }
        else if (level < 800)
        {
            path = "lp19/lvl" + (level % 76).ToString();

        }
        else if (level < 840)
        {
            path = "lp20/lvl" + (level % 80).ToString();
        }
        else if (level < 880)
        {
            path = "lp21/lvl" + (level % 84).ToString();
        }
        else if (level < 920)
        {
            path = "lp22/lvl" + (level % 88).ToString();
        }
        else if (level < 960)
        {
            path = "lp23/lvl" + (level % 92).ToString();
        }
        else if (level < 1000)
        {
            path = "lp1000/lvl" + (level % 97).ToString();
        }
        else if (level < 1030)
        {
            path = "lp1001/lvl" + (level % 100).ToString();
        }
        else if (level < 1060)
        {
            path = "lp1002/lvl" + (level % 103).ToString();
        }
        else
        {
            path = "lp1003/lvl" + (level % 106).ToString();
        }

        var ta = Resources.Load(path);
        if (ta != null)
        {
            dataLevel = JsonUtility.FromJson<Level>(ta.ToString());
            return dataLevel;
        }
        else return null;
    }

    public bool IsHasData(int level)
    {
        var data = LoadLevelChallenge(level);
        if (data == null) return false;
        return true;
    }

    public int GetMaxLevelUnlock()
    {
        return ServicesManager.DataSecure().GetInt("MaxLevelUnlock", 1);
    }

    public bool GetSoundOnOff()
    {
        return ServicesManager.DataSecure().GetBool("IsSoundOn", true);
    }

    public bool GetDidTut()
    {
        return ServicesManager.DataSecure().GetBool("IsDidTut", false);
    }

    public int GetMoney()
    {
        return ServicesManager.DataSecure().GetInt("Money", 0);
    }

    public void SaveData()
    {
        ServicesManager.DataSecure().SetInt("MaxLevelUnlock", maxLevelUnlock);
        ServicesManager.DataSecure().SetBool("IsSoundOn", m_IsSoundOn);
        ServicesManager.DataSecure().SetInt("Money", money);
        ServicesManager.DataSecure().SetBool("IsDidTut", isDidTut);
    }

    public int GetMaxLevelUnlockInHard()
    {
        return ServicesManager.DataSecure().GetInt("MaxLevelUnlockInHard", 1);
    }
}
