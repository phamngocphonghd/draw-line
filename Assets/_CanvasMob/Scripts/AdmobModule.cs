﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GoogleMobileAds.Api;
namespace Root
{
    public class AdmobModule : IAdsService
    {
        Action m_CloseCallback;
        Action m_LoadedCallback;
        InterstitialAd m_AppotaxInterstitial;

        private BannerView bannerView;
        private BannerView bannerViewPause;
        private BannerView bannerViewMrec;
        string androidInterID;
        string iosInterID;
        string androidBannerID;
        string iosBannerID;
        string androidMrecID;
        string iosMrecID;

        bool isAcceptConsent = true;

        public void Init(AdsConfig adsConfig)
        {
            androidInterID = adsConfig.AndroidInterstitialID;
            iosInterID = adsConfig.IosInterstitialID;
            androidBannerID = adsConfig.AndroidBannerID;
            iosBannerID = adsConfig.IosBannerID;
        }

        public void Init(AdsConfig adsConfig, bool isAcceptConsent)
        {
            androidInterID = adsConfig.AndroidInterstitialID;
            iosInterID = adsConfig.IosInterstitialID;
            androidBannerID = adsConfig.AndroidBannerID;
            iosBannerID = adsConfig.IosBannerID;
            this.isAcceptConsent = isAcceptConsent;
        }

        public void SetConsent(bool isAcceptConsent)
        {
            this.isAcceptConsent = isAcceptConsent;
        }

        public void ShowFullAds(Action closeCallback = null)
        {
            m_CloseCallback = closeCallback;
            if (IsFullAdsLoaded())
            {
                m_AppotaxInterstitial.Show();
            }
            else
            {
                if (m_CloseCallback != null)
                {
                    m_CloseCallback.Invoke();
                }
            }
            LoadFullAds();
        }

        public void LoadFullAds(Action loadedCallback = null)
        {
#if UNITY_ANDROID
            string adUnitId = androidInterID;
#elif UNITY_IOS
        string adUnitId = iosInterID;
#else
        string adUnitId = "unexpected_platform";
#endif

            m_AppotaxInterstitial = new InterstitialAd(adUnitId);
            m_AppotaxInterstitial.OnAdClosed += HandleOnAdClosed;
            AdRequest request;
            if (isAcceptConsent)
            {
                request = new AdRequest.Builder().Build();
            }
            else
            {
                request = new AdRequest.Builder().AddExtra("npa", "1").Build();
            }
            // Load the interstitial with the request.
            m_AppotaxInterstitial.LoadAd(request);
        }

        void HandleOnAdClosed(object sender, EventArgs args)
        {
            if (m_CloseCallback != null)
            {
                m_CloseCallback.Invoke();
            }
        }
        public bool IsFullAdsLoaded()
        {
            if (m_AppotaxInterstitial == null) return false;
            return m_AppotaxInterstitial.IsLoaded();
        }

        public void LoadAndShowBannerBot()
        {

        }

        public void LoadAndShowBannerTop()
        {

        }

        public void LoadBannerBot()
        {
#if UNITY_ANDROID
            string adUnitId = androidBannerID;
#elif UNITY_IPHONE
            string adUnitId = iosBannerID;
#else
            string adUnitId = "unexpected_platform";
#endif

            // Create a 320x50 banner at the top of the screen.
            bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

            // Create an empty ad request.
            AdRequest request;
            if (isAcceptConsent)
            {
                request = new AdRequest.Builder().Build();
            }
            else
            {
                request = new AdRequest.Builder().AddExtra("npa", "1").Build();
            }

            // Load the banner with the request.
            bannerView.LoadAd(request);

            Debug.Log("Load banner appotax");
            HideBanner();
        }

        public void LoadBannerTop()
        {

        }

        public void LoadBannerTopForPause()
        {
#if UNITY_ANDROID
            string adUnitId = androidBannerID;
#elif UNITY_IPHONE
            string adUnitId = iosBannerID;
#else
            string adUnitId = "unexpected_platform";
#endif

            // Create a 320x50 banner at the top of the screen.

            bannerViewPause = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Top);

            // Create an empty ad request.
            AdRequest request;
            if (isAcceptConsent)
            {
                request = new AdRequest.Builder().Build();
            }
            else
            {
                request = new AdRequest.Builder().AddExtra("npa", "1").Build();
            }

            // Load the banner with the request.
            bannerViewPause.LoadAd(request);

            Debug.Log("Load banner appotax");
            HideBannerPause();
        }

        public void LoadBannerMrec()
        {
#if UNITY_ANDROID
            string adUnitId = androidMrecID;
#elif UNITY_IPHONE
            string adUnitId = iosMrecID;
#else
            string adUnitId = "unexpected_platform";
#endif
            bannerViewMrec = new BannerView(adUnitId, AdSize.MediumRectangle, AdPosition.Center);

            AdRequest request;
            if (isAcceptConsent)
            {
                request = new AdRequest.Builder().Build();
            }
            else
            {
                request = new AdRequest.Builder().AddExtra("npa", "1").Build();
            }

            // Load the banner with the request.
            bannerViewMrec.LoadAd(request);

            Debug.Log("Load mrec banner appotax");
            HideMrecBanner();
        }

        public void ShowMrecBanner()
        {
            if (bannerViewMrec != null)
            {
                //Debug.Log("Show banner appotax");
                bannerViewMrec.Show();
            }
        }

        public void ShowBanner(bool isRefreshBanner = false)
        {
            if (bannerView != null)
            {
                //Debug.Log("Show banner appotax");
                bannerView.Show();
            }
        }

        public void ShowBannerPause()
        {
            if (bannerViewPause != null)
            {
                bannerViewPause.Show();
            }
        }

        public void HideMrecBanner()
        {
            if (bannerViewMrec != null)
            {
                bannerViewMrec.Hide();
            }
        }

        public void HideBanner()
        {
            if (bannerView != null)
            {
                //Debug.Log("Show banner appotax");
                bannerView.Hide();
            }
        }

        public void HideBannerPause()
        {
            if (bannerViewPause != null)
            {
                bannerViewPause.Hide();
            }
        }

        public void RefreshBanner()
        {
            AdRequest request = new AdRequest.Builder().Build();
            if (bannerView != null)
            {
                bannerView.LoadAd(request);
            }
            HideBanner();
        }

        public void RefreshBannerPause()
        {
            if (bannerViewPause != null)
            {
                AdRequest request = new AdRequest.Builder().Build();
                bannerViewPause.LoadAd(request);
                HideBannerPause();
            }
            else
            {
                LoadBannerTopForPause();
            }
        }

        public void ShutDown()
        {

        }
    }
}

