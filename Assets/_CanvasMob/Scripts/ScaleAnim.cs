﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.ZestKit;

public class ScaleAnim : MonoBehaviour
{
    public Vector3 ScaleDes;
    public EaseType type;
    public float scale_size;
    public float duration;
    ITween<Vector3> animTween;
    // Use this for initialization
    public void StartAnim()
    {
        transform.localScale = Vector3.one*scale_size;
        animTween = transform.ZKlocalScaleTo(ScaleDes, duration)
            .setEaseType(type)
        .setLoops(LoopType.PingPong, 1000);
        animTween.start();
    }

    // Update is called once per frame
    public void StopAnim()
    {
        if(animTween != null)
        {
            animTween.stop();
        }
        animTween = null;
    }
}
