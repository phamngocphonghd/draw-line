﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ShowFPS : MonoBehaviour {

    public float f_UpdateInterval = 0.5F;

    private float f_LastInterval;

    private int i_Frames = 0;

	private float f_Fps;
	
	int halfScrH;
	int size;
	int size2;
	string format = "00";

    public Text text;

    static bool isInstance = false;

    void Start() 
    {
        //Application.targetFrameRate = 70;
        if(isInstance)
        {
            Destroy(gameObject);
        }
        isInstance = true;
        DontDestroyOnLoad(gameObject);

        f_LastInterval = Time.realtimeSinceStartup;

	    i_Frames = 0;
	    
	    halfScrH = Screen.height/2;
	    size = Screen.height/10;
	    size2 = size*2;
    }

    void Update() 
    {
        ++i_Frames;

        if (Time.realtimeSinceStartup > f_LastInterval + f_UpdateInterval) 
        {
            f_Fps = i_Frames / (Time.realtimeSinceStartup - f_LastInterval);

            i_Frames = 0;

            f_LastInterval = Time.realtimeSinceStartup;

            text.text = GetIntString(Mathf.RoundToInt(f_Fps));
        }
    }

    static Dictionary<int, string> _intStrings = new Dictionary<int, string>();

    public static string GetIntString(int number)
    {
        if (!_intStrings.ContainsKey(number))
        {
            _intStrings[number] = number.ToString();
        }
        return _intStrings[number];
    }
}
