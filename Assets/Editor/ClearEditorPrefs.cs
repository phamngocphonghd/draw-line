﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// Clear all the editor prefs keys.
	//
	// Warning: this will also remove editor preferences as the opened projects, etc

	class ClearEditorPrefs : ScriptableObject {
		
		[MenuItem ("Hale/Clear all Editor Preferences")]
		public static void DoDeselect() {
			
			if(EditorUtility.DisplayDialog("Delete all editor preferences?",
				"Are you sure you want to delete all the editor preferences?," +
				"this action cannot be undone.",
				"Yes",
				"No"))
				PlayerPrefs.DeleteAll();
		}
	}
